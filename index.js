
var app = require("express")();
var http = require("http").createServer(app);
var io = require("socket.io")(http);
const {joinUser} = require('./users');
app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});
io.on("connection", function (socket) {
  console.log("connected");
  socket.on("join room", (data) => {
    console.log('in room');
    let Newuser = joinUser(socket.id, data.username)
    //io.to(Newuser.roomname).emit('send data' , {username : Newuser.username,roomname : Newuser.roomname, id : socket.id})
   // io.to(socket.id).emit('send data' , {id : socket.id ,username:Newuser.username, roomname : Newuser.roomname });
   socket.emit('send data' , {id : socket.id ,username:Newuser.username});
   io.emit('is_online', '<i>'+ 'You are </i>' + Newuser.username);
   
    console.log(Newuser);
  });
  socket.on("chat message", (data) => {
    io.emit("chat message", {data:data,id : socket.id});
  });
});

http.listen(3000, function () {});